#ifndef PKSLIDER_H
#define PKSLIDER_H

#include <QTimer>
#include <QWidget>
#include <memory>

#include "calc/scalecalc.h"

class PKSlider : public QWidget {
	Q_OBJECT

	Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
	Q_PROPERTY(double maximum READ maximum WRITE setMaximum)

	Q_PROPERTY(double singleStep READ singleStep WRITE setSingleStep)
	Q_PROPERTY(double pageStep READ pageStep WRITE setPageStep)
	Q_PROPERTY(double defaultValue READ defaultValue WRITE setDefaultValue)
	Q_PROPERTY(double value READ value WRITE setValue)
	Q_PROPERTY(int numberOfMarkers READ countMarkers WRITE setCountMarkers)
	Q_PROPERTY(bool showMarker READ drawMarker WRITE setDrawMarker)
	Q_PROPERTY(bool displayValue READ displayValue WRITE setDisplayValue)
	Q_PROPERTY(int displayValueTimeout READ displayValueTimeout WRITE
				   setDisplayValueTimeout)
	Q_PROPERTY(QColor KnobColor READ colorKnob WRITE setColorKnob)
	Q_PROPERTY(QString unit READ unit WRITE setUnit)

  public:
	explicit PKSlider(QWidget *parent = nullptr);

	void setScaleCalc(std::shared_ptr<ScaleCalc> scaleCalc);

	float minimum() const;
	void setMinimum(float fMinimum);

	float maximum() const;
	void setMaximum(float fMmaximum);

	float value() const;
	void setValue(float fValue);

	QColor colorKnob() const;
	void setColorKnob(const QColor &colorKnob);

	bool drawMarker() const;
	void setDrawMarker(bool drawMarker);

	float defaultValue() const;
	void setDefaultValue(float defaultValue);

	bool drawNotch() const;
	void setDrawNotch(bool drawNotch);

	int countMarkers() const;
	void setCountMarkers(int countMarkers);

	float singleStep() const;
	void setSingleStep(float singleStep);

	float pageStep() const;
	void setPageStep(float pageStep);

	bool displayValue() const;
	void setDisplayValue(bool displayValue);

	int displayValueTimeout() const;
	void setDisplayValueTimeout(int displayValueTimeout);

	QString unit() const;
	void setUnit(const QString &unit);

  signals:

	// QWidget interface
  protected:
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void wheelEvent(QWheelEvent *event) override;
	virtual void keyPressEvent(QKeyEvent *event) override;

  private: // methods
	void initValues();
	void initValueDisplay();

	unsigned int calculatePosition(float value);
	float positionToValue(int pos);
	void drawScalecalcScale(QPainter &painter);
	void drawCustomScale(QPainter &painter);
	void drawCurrentValue(QPaintEvent *event, QPainter &painter);
	void drawValueText(QPainter &painter);

  private: // variables
	QTimer m_TimerMouse;
	QTimer m_TimerValue;
	std::shared_ptr<ScaleCalc> m_pScaleCalc = nullptr;

	// geometry
	QRect m_RectBounds;
	QRect m_RectSlideSlot;
	QLinearGradient m_GradientSlot;
	QPainterPath m_PathSlideSlot;
	int m_iMarkerWidth = 2;
	unsigned int m_iSlotWidth = 8;
	unsigned int m_iBorderTop = 6;
	unsigned int m_iBorderBottom = 6;
	unsigned int m_iSliderHeight = 6;
	unsigned int m_iSliderWith = 18;
	unsigned int m_iFontSizeValue = 6;
	bool m_bDrawValue = false;

	// display
	bool m_bDrawMarker = true;
	int m_iCountMarkers = 10;
	bool m_bDrawMarkerValues = true;
	bool m_bDisplayValue = true;
	int m_iDisplayValueTimeout = 500;
	QString m_sUnit;

	// color
	QColor m_color1Slot = QColor(0, 0, 0);
	QColor m_color2Slot = QColor(180, 180, 180);
	QColor m_ColorKnob = QColor(155, 167, 187, 200);
	QColor m_ColorMarker = QColor(255, 255, 255);

	unsigned int m_iScaleRange = 0;

	// steps
	float m_fSingleStep = 1.f;
	float m_fPageStep = 10.f;

	// values
	float m_fMinimum = 0;
	float m_fMaximum = 100;
	float m_fValueRange;
	float m_fMapping;
	float m_fDefaultValue = 0;
	float m_fValue = 10;
	float m_fNextValue;

  private slots:
	void mouseTimerElapsed();

  signals:
	void rangeChanged(float min, float max);
	void valueChanged(float value);
	void sliderPressed();
	void sliderReleased();
};

#endif // PKSLIDER_H
