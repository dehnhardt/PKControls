#include "pklevelmeter.h"

#include <QPainter>
#include <iostream>

PKLevelMeter::PKLevelMeter(QWidget *parent) : QWidget(parent) {
	m_timerHold.setInterval(1000);
	m_timerHold.setSingleShot(true);
	connect(&m_timerHold, &QTimer::timeout, [=] { m_fCurrentMax = m_fLevel; });
	initValues();
}

PKLevelMeter::~PKLevelMeter() {}

void PKLevelMeter::setScaleCalc(std::shared_ptr<ScaleCalc> scaleCalc) {
	this->m_pScaleCalc = scaleCalc;
	setLevelMin(static_cast<int>(scaleCalc->minAllowedValue()));
	setLevelMax(static_cast<int>(scaleCalc->maxAllowedValue()));
	m_fLevel = m_fLevelMin;
	update();
}

void PKLevelMeter::reset() {
	m_fLevel = m_fLevelMin;
	m_bOverload = false;
	update();
}

float PKLevelMeter::level() const {
	if (m_pScaleCalc)
		return m_pScaleCalc->decode(m_fLevel);
	return m_fLevel;
}

void PKLevelMeter::setLevel(float level) {

	if (m_pScaleCalc) {
		level = m_pScaleCalc->encode(level);
	}

	if (level == m_fLevel)
		return;

	if (level < m_fLevelMin)
		level = m_fLevelMin;
	else if (level > m_fLevelMax)
		m_fLevel = m_fLevelMax;
	else
		m_fLevel = level;
	if (m_bShowOverloadSignal && (level > m_fLevelCritical) &&
		m_bShowCriticalColor)
		m_bOverload = true;
	if (m_bHoldMaxValue && (level > m_fCurrentMax)) {
		m_fCurrentMax = level;
		m_timerHold.start();
	}
	update();
}

bool PKLevelMeter::showCriticalColor() const { return m_bShowCriticalColor; }

void PKLevelMeter::setShowCriticalColor(bool bShowCriticalColor) {
	m_bShowCriticalColor = bShowCriticalColor;
	update();
}

bool PKLevelMeter::showWarningColor() const { return m_bShowWarningColor; }

void PKLevelMeter::setShowWarningColor(bool bShowWarningColor) {
	m_bShowWarningColor = bShowWarningColor;
	update();
}

float PKLevelMeter::levelCritical() const {
	if (m_pScaleCalc)
		return m_pScaleCalc->decode(m_fLevelCritical);
	return m_fLevelCritical;
}

void PKLevelMeter::setLevelCritical(float fLevelCritical) {
	if (m_pScaleCalc)
		m_fLevelCritical = m_pScaleCalc->encode(fLevelCritical);
	else
		m_fLevelCritical = fLevelCritical;
	update();
}

float PKLevelMeter::levelWarning() const {
	if (m_pScaleCalc)
		return m_pScaleCalc->decode(m_fLevelWarning);
	return m_fLevelWarning;
}

void PKLevelMeter::setLevelWarning(float fLevelWarning) {
	if (m_pScaleCalc)
		m_fLevelWarning = m_pScaleCalc->encode(fLevelWarning);
	else
		m_fLevelWarning = fLevelWarning;
	update();
}

float PKLevelMeter::levelMin() const { return m_fLevelMin; }

void PKLevelMeter::setLevelMin(float levelMin) {
	m_fLevelMin = levelMin;
	initValues();
}

float PKLevelMeter::levelMax() const { return m_fLevelMax; }

void PKLevelMeter::setLevelMax(float levelMax) {
	m_fLevelMax = levelMax;
	initValues();
}

void PKLevelMeter::paintSection(QPainter &painter, QColor &color, float fromVal,
								float toVal) {

	float pos2 = calcPosition(toVal);
	float pos1 = calcPosition(fromVal);

	QRect rect(m_RectMeter.left(), static_cast<int>(pos1), m_RectMeter.width(),
			   static_cast<int>(pos2 - pos1));
	QBrush b = QBrush(color);
	painter.fillRect(rect, b);
}

void PKLevelMeter::initValues() {
	if (m_fLevelMin > m_fLevelMax)
		qSwap(m_fLevelMin, m_fLevelMax);
	m_fRange = this->m_fLevelMax - this->m_fLevelMin;
	m_fCurrentMax = m_fLevelMin;
	update();
}

int PKLevelMeter::calcPosition(float level) {
	float val = level / m_fRange;
	float pos = m_RectMeter.height() - (m_RectMeter.height() * val);
	return static_cast<int>(pos + m_RectMeter.y());
}

bool PKLevelMeter::holdMaxValue() const { return m_bHoldMaxValue; }

void PKLevelMeter::resizeEvent(__attribute__((unused)) QResizeEvent *event) {
	m_RectMeter = rect().adjusted(+1, +6, -1, -1);
	m_RectInnerMeter = m_RectMeter.adjusted(+1, +1, 0, 0);
	m_RectOverload = rect().adjusted(1, 1, -1, -1);
	m_RectOverload.setHeight(5);
	m_RectInnerOverload = m_RectOverload.adjusted(+1, +1, 0, 0);
}

void PKLevelMeter::paintEvent(__attribute__((unused)) QPaintEvent *event) {
	QPainter painter(this);
	QPalette palette;
	QPen p = painter.pen();
	painter.setPen(QPalette::Shadow);
	painter.setBrush(palette.window());
	painter.drawRect(m_RectMeter);
	painter.drawRect(m_RectOverload);
	if (m_bShowOverloadSignal && m_bOverload)
		painter.fillRect(m_RectInnerOverload, m_ColorCritical);

	float fromValue = m_fLevelMin;
	float toValue = m_fLevel;
	if (showCriticalColor() && (m_fLevel > m_fLevelCritical)) {
		fromValue = m_fLevelCritical;
		paintSection(painter, m_ColorCritical, fromValue, toValue);
		toValue = m_fLevelCritical;
	}
	if (showWarningColor() && m_fLevel > m_fLevelWarning) {
		fromValue = m_fLevelWarning;
		paintSection(painter, m_ColorWarning, fromValue, toValue);
		toValue = m_fLevelWarning;
	}
	fromValue = m_fLevelMin;
	paintSection(painter, m_ColorPlain, fromValue, toValue);
	painter.setPen(p);
	if (m_bHoldMaxValue && (m_fCurrentMax > m_fLevelMin)) {
		int pos = calcPosition(m_fCurrentMax);
		painter.drawLine(m_RectInnerMeter.left(), pos, m_RectInnerMeter.right(),
						 pos);
	}
	painter.end();
}

void PKLevelMeter::setHoldMaxValue(bool bHoldMaxValue) {
	m_bHoldMaxValue = bHoldMaxValue;
	update();
}

bool PKLevelMeter::showOverloadSignal() const { return m_bShowOverloadSignal; }

void PKLevelMeter::setShowOverloadSignal(bool bShowOverloadSignal) {
	m_bShowOverloadSignal = bShowOverloadSignal;
	update();
}

QColor PKLevelMeter::colorPlain() const { return m_ColorPlain; }

void PKLevelMeter::setColorPlain(const QColor &ColorPlain) {
	m_ColorPlain = ColorPlain;
	update();
}

QColor PKLevelMeter::colorWarning() const { return m_ColorWarning; }

void PKLevelMeter::setColorWarning(const QColor &ColorWarning) {
	m_ColorWarning = ColorWarning;
	update();
}

QColor PKLevelMeter::colorCritical() const { return m_ColorCritical; }

void PKLevelMeter::setColorCritical(const QColor &ColorCritical) {
	m_ColorCritical = ColorCritical;
	update();
}

void PKLevelMeter::mousePressEvent(QMouseEvent *event) {
	if (m_RectOverload.contains(event->pos()) && m_bOverload)
		m_bOverload = false;
	update();
}
