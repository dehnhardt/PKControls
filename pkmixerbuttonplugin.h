#ifndef PKMIXERBUTTONPLUGIN_H
#define PKMIXERBUTTONPLUGIN_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>

class PKMixerButtonPlugin : public QObject,
							public QDesignerCustomWidgetInterface {
	Q_OBJECT
	Q_INTERFACES(QDesignerCustomWidgetInterface)

  public:
	PKMixerButtonPlugin(QObject *parent = nullptr);

	bool isContainer() const;
	bool isInitialized() const;
	QIcon icon() const;
	QString domXml() const;
	QString group() const;
	QString includeFile() const;
	QString name() const;
	QString toolTip() const;
	QString whatsThis() const;
	QWidget *createWidget(QWidget *parent);
	void initialize(QDesignerFormEditorInterface *core);

  private:
	bool m_initialized;
};

#endif // PKMIXERBUTTONPLUGIN_H
