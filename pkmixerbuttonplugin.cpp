#include "pkmixerbuttonplugin.h"
#include "pkmixerbutton.h"

#include <QtPlugin>

PKMixerButtonPlugin::PKMixerButtonPlugin(QObject *parent) : QObject(parent) {
	m_initialized = false;
}

void PKMixerButtonPlugin::initialize(
	QDesignerFormEditorInterface * /* core */) {
	if (m_initialized)
		return;

	// Add extension registrations, etc. here

	m_initialized = true;
}

bool PKMixerButtonPlugin::isInitialized() const { return m_initialized; }

QWidget *PKMixerButtonPlugin::createWidget(QWidget *parent) {
	return new PKMixerButton(parent);
}

QString PKMixerButtonPlugin::name() const {
	return QLatin1String("PKMixerButton");
}

QString PKMixerButtonPlugin::group() const {
	return QLatin1String("PKWidgets");
}

QIcon PKMixerButtonPlugin::icon() const { return QIcon(":/PKMixerButton.xpm"); }

QString PKMixerButtonPlugin::toolTip() const {
	return QLatin1String("Create a shine translucent toolbutton");
}

QString PKMixerButtonPlugin::whatsThis() const {
	return QLatin1String("A toolbutton with custom colors\n\t");
}

bool PKMixerButtonPlugin::isContainer() const { return false; }

QString PKMixerButtonPlugin::domXml() const {
	return QLatin1String(
		"<widget class=\"PKMixerButton\" name=\"pkMixerButton\">\n</widget>\n");
}

QString PKMixerButtonPlugin::includeFile() const {
	return QLatin1String("pkmixerbutton.h");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(pklevelmeterplugin, PKLevelMeterPlugin)
#endif // QT_VERSION < 0x050000
