#include "pklevelmeterplugin.h"
#include "pklevelmeter.h"

#include <QtPlugin>

PKLevelMeterPlugin::PKLevelMeterPlugin(QObject *parent) : QObject(parent) {
	m_initialized = false;
}

void PKLevelMeterPlugin::initialize(QDesignerFormEditorInterface * /* core */) {
	if (m_initialized)
		return;

	// Add extension registrations, etc. here

	m_initialized = true;
}

bool PKLevelMeterPlugin::isInitialized() const { return m_initialized; }

QWidget *PKLevelMeterPlugin::createWidget(QWidget *parent) {
	return new PKLevelMeter(parent);
}

QString PKLevelMeterPlugin::name() const {
	return QLatin1String("PKLevelMeter");
}

QString PKLevelMeterPlugin::group() const { return QLatin1String("PKWidgets"); }

QIcon PKLevelMeterPlugin::icon() const { return QIcon(":/PKLevelMeter.xpm"); }

QString PKLevelMeterPlugin::toolTip() const {
	return QLatin1String("Create a configurable levelmeter");
}

QString PKLevelMeterPlugin::whatsThis() const {
	return QLatin1String("A levelmeter with the possibility to signal "
						 "overloads and hold maximum values\n\t");
}

bool PKLevelMeterPlugin::isContainer() const { return false; }

QString PKLevelMeterPlugin::domXml() const {
	return QLatin1String(
		"<widget class=\"PKLevelMeter\" name=\"pKLevelMeter\">\n</widget>\n");
}

QString PKLevelMeterPlugin::includeFile() const {
	return QLatin1String("pklevelmeter.h");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(pklevelmeterplugin, PKLevelMeterPlugin)
#endif // QT_VERSION < 0x050000
