#ifndef PKPLUGINCOLLECTION_H
#define PKPLUGINCOLLECTION_H

#include <QtUiPlugin/QDesignerCustomWidgetInterface>

class PKPluginCollection : public QObject,
						   public QDesignerCustomWidgetCollectionInterface {
	Q_OBJECT
#if QT_VERSION >= 0x050000
	Q_PLUGIN_METADATA(
		IID "org.qt-project.Qt.QDesignerCustomWidgetCollectionInterface")
	Q_INTERFACES(QDesignerCustomWidgetCollectionInterface)
#endif // QT_VERSION >= 0x050000

  public:
	PKPluginCollection(QObject *parent = nullptr);

	QList<QDesignerCustomWidgetInterface *> customWidgets() const override;

  private:
	QList<QDesignerCustomWidgetInterface *> widgets;
};

#endif // PKPLUGINCOLLECTION_H
