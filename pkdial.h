#ifndef PKDIAL_H
#define PKDIAL_H

#include <QAbstractSlider>
#include <QObject>
#include <QTimer>
#include <QWheelEvent>
#include <QPainter>
#include <QPainterPath>
#include <memory>

#include "calc/scalecalc.h"

class PKDial : public QWidget {
	Q_OBJECT

	Q_PROPERTY(double minimum READ minimum WRITE setMinimum)
	Q_PROPERTY(double maximum READ maximum WRITE setMaximum)
	Q_PROPERTY(int scaleAnchor READ scaleAnchor WRITE setScaleAnchor)
	Q_PROPERTY(int scaleRange READ scaleRange WRITE setScaleRange NOTIFY
				   scaleRangeChanged)
	Q_PROPERTY(double singleStep READ singleStep WRITE setSingleStep)
	Q_PROPERTY(double pageStep READ pageStep WRITE setPageStep)
	Q_PROPERTY(double defaultValue READ defaultValue WRITE setDefaultValue)
	Q_PROPERTY(double value READ value WRITE setValue)
	Q_PROPERTY(int numberOfMarkers READ countMarkers WRITE setCountMarkers)
	Q_PROPERTY(bool showMarker READ drawMarker WRITE setDrawMarker)
	Q_PROPERTY(bool showNotch READ drawNotch WRITE setDrawNotch)
	Q_PROPERTY(bool displayValue READ displayValue WRITE setDisplayValue)
	Q_PROPERTY(int displayValueTimeout READ displayValueTimeout WRITE
				   setDisplayValueTimeout)
	Q_PROPERTY(QColor KnobColor READ colorKnob WRITE setColorKnob)
	Q_PROPERTY(QString unit READ unit WRITE setUnit)

  public:
	PKDial(QWidget *widget = nullptr);

	void setScaleCalc(std::shared_ptr<ScaleCalc> scaleCalc);

	float minimum() const;
	void setMinimum(float minimum);

	float maximum() const;
	void setMaximum(float maximum);

	unsigned int scaleAnchor() const;
	void setScaleAnchor(unsigned int scaleAnchor);

	unsigned int scaleRange() const;
	void setScaleRange(unsigned int scaleRange);

	unsigned int scaleOffset() const;

	float value() const;
	void setValue(float value);

	QColor colorKnob() const;
	void setColorKnob(const QColor &colorKnob);

	bool drawMarker() const;
	void setDrawMarker(bool drawMarker);

	float defaultValue() const;
	void setDefaultValue(float defaultValue);

	bool drawNotch() const;
	void setDrawNotch(bool drawNotch);

	int countMarkers() const;
	void setCountMarkers(int countMarkers);

	float singleStep() const;
	void setSingleStep(float singleStep);

	float pageStep() const;
	void setPageStep(float pageStep);

	bool displayValue() const;
	void setDisplayValue(bool displayValue);

	int displayValueTimeout() const;
	void setDisplayValueTimeout(int displayValueTimeout);

	QString unit() const;
	void setUnit(const QString &unit);

  protected:
	// QWidget interface
	virtual void wheelEvent(QWheelEvent *event) override;
	virtual void paintEvent(QPaintEvent *event) override;
	virtual void resizeEvent(QResizeEvent *event) override;
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void keyPressEvent(QKeyEvent *event) override;

  private: // methods
	void initValues();
	void initValueDisplay();
	double calculateAngle(float val);
	float mousePositionToAngle(QMouseEvent *event);
	float angleToValue(float angle);
	void drawScalecalcScale(QPainter &painter);
	void drawDefaultScale(QPainter &painter);
	void drawCustomScale(QPainter &painter);
	void drawCurrentValue(QPaintEvent *event, QPainter &painter);
	void drawValueText(QPainter &painter);

  private: // variables
	QTimer m_TimerMouse;
	QTimer m_TimerValue;
	std::shared_ptr<ScaleCalc> m_pScaleCalc = nullptr;

	// geometry
	QRect m_RectContent;
	QRect m_RectDial;
	QPainterPath m_DialPath;
	QPoint m_PointCenter;
	int m_iMarkerWidth = 1;
	int m_iNotchLineWidth = 1;
	int m_iNotchMarkerWidth = 1;
	int m_iFontSizeValue;
	int m_iFontSizeMarker;
	bool m_bDrawNotchLine = true;
	bool m_bDrawValue = false;

	// display
	bool m_bDrawMarker = true;
	bool m_bDrawNotch = true;
	int m_iCountMarkers = 0;
	bool m_bDisplayValue = false;
	int m_iDisplayValueTimeout = 0;
	QString m_sUnit;

	// steps
	float m_fSingleStep = 1.f;
	float m_fPageStep = 10.f;

	// color
	QColor m_ColorKnob = QColor(155, 167, 187, 200);
	// QColor m_ColorMarker = QColor(0, 0, 0);
	QColor m_ColorMarker = QColor(255, 255, 255);
	QColor m_ColorNotch = QColor(188, 198, 204);

	// scale
	unsigned int m_iScaleAnchor = 0;
	unsigned int m_iScaleRange = 300;
	unsigned int m_iScaleOffset = 0;

	// values
	float m_fMinimum = 0;
	float m_fMaximum = 100;
	float m_fValueRange;
	float m_fMapping;
	float m_fDefaultValue = 0;
	float m_fValue = 10;
	float m_fNextValue;

  private slots:
	void mouseTimerElapsed();

  signals:
	void rangeChanged(float min, float max);
	void valueChanged(float value);
	void dialPressed();
	void dialReleased();
	void scaleAnchorChanged(unsigned int anchor);
	void scaleRangeChanged(unsigned int scaleRange);
};

#endif // PKDIAL_H
