#include "pkplugincollection.h"
#include "pkdialplugin.h"
#include "pklevelmeterplugin.h"
#include "pkmixerbuttonplugin.h"
#include "pksliderplugin.h"

PKPluginCollection::PKPluginCollection(QObject *parent) : QObject(parent) {

	widgets.append(new PKLevelMeterPlugin(this));
	widgets.append(new PKDialPlugin(this));
	widgets.append(new PKMixerButtonPlugin(this));
	widgets.append(new PKSliderPlugin(this));
}

QList<QDesignerCustomWidgetInterface *>
PKPluginCollection::customWidgets() const {
	return widgets;
}
