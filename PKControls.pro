CONFIG      += plugin debug_and_release
TARGET      = $$qtLibraryTarget(pkcontrolsplugin)
TEMPLATE    = lib

HEADERS     = pklevelmeterplugin.h \
    pkdialplugin.h \
	pkmixerbuttonplugin.h \
	pkplugincollection.h \
	pksliderplugin.h

SOURCES     = pklevelmeterplugin.cpp \
    pkdialplugin.cpp \
	pkmixerbuttonplugin.cpp \
	pkplugincollection.cpp \
	pksliderplugin.cpp

RESOURCES   = icons.qrc
LIBS        += -L.

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += designer
} else {
    CONFIG += designer
}

target.path = $$[QT_INSTALL_PLUGINS]/designer
INSTALLS    += target

include(pklevelmeter.pri)
include(pkdial.pri)
include(pkslider.pri)
include(pkmixerbutton.pri)

QT += widgets

DISTFILES += \
    calc.pri \
	pkmixerbutton.pri \
	pklevelmeter.pri \
	pkdial.pri \
	pkslider.pri


FORMS +=
