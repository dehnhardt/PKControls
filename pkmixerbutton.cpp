#include "pkmixerbutton.h"

#include <QPaintEvent>
#include <QPainter>
#include <iostream>
#include <math.h>
#include <sstream>

PKMixerButton::PKMixerButton(QWidget *parent) : QToolButton(parent) {
	setCheckable(true);
	init();
}

QColor PKMixerButton::activeColor() { return m_ColorActive; }

void PKMixerButton::setActiveColor(QColor color) {
	m_ColorActive = color;
	init();
}

void PKMixerButton::init() {
	std::stringstream ss;
	ss << "QToolButton { border: 1px solid #eeeeee; border-radius: 3px; "
		  "background-color: rgb(255,255,255);color: rgb(50,50,50);}";
	ss << "QToolButton:checked { border:none; background: "
		  "qradialgradient(cx:0, cy:0, "
		  "radius: 1, fx:0.5, fy:0.5, stop:0 "
		  "white, stop:1 rgb("
	   << m_ColorActive.red() << "," << m_ColorActive.green() << ","
	   << m_ColorActive.blue() << ") ) }";
	setStyleSheet(ss.str().c_str());
	update();
}
