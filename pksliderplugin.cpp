#include "pksliderplugin.h"
#include "pkslider.h"

#include <QtPlugin>

PKSliderPlugin::PKSliderPlugin(QObject *parent) : QObject(parent) {
	m_initialized = false;
}

void PKSliderPlugin::initialize(QDesignerFormEditorInterface * /* core */) {
	if (m_initialized)
		return;

	// Add extension registrations, etc. here

	m_initialized = true;
}

bool PKSliderPlugin::isInitialized() const { return m_initialized; }

QWidget *PKSliderPlugin::createWidget(QWidget *parent) {
	return new PKSlider(parent);
}

QString PKSliderPlugin::name() const { return QLatin1String("PKSlider"); }

QString PKSliderPlugin::group() const { return QLatin1String("PKWidgets"); }

QIcon PKSliderPlugin::icon() const { return QIcon(); }

QString PKSliderPlugin::toolTip() const {
	return QLatin1String("Create a configurable slider");
}

QString PKSliderPlugin::whatsThis() const {
	return QLatin1String(
		"A slider with inside markers and the possibility to add a"
		"calculater\n\t");
}

bool PKSliderPlugin::isContainer() const { return false; }

QString PKSliderPlugin::domXml() const {
	return QLatin1String(
		"<widget class=\"PKSlider\" name=\"pKSlider\">\n</widget>\n");
}

QString PKSliderPlugin::includeFile() const {
	return QLatin1String("pkslider.h");
}
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(pkdialplugin, PKSliderPlugin)
#endif // QT_VERSION < 0x050000
