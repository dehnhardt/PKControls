#include "pkslider.h"

#include <QPainter>
#include <QWheelEvent>
#include <cmath>
#include <iostream>

PKSlider::PKSlider(QWidget *parent) : QWidget(parent) {
	setFocusPolicy(Qt::StrongFocus);
	m_TimerMouse.setSingleShot(true);
	m_TimerMouse.setInterval(250);
	m_TimerValue.setSingleShot(true);
	connect(&m_TimerMouse, &QTimer::timeout, this,
			&PKSlider::mouseTimerElapsed);
	connect(&m_TimerValue, &QTimer::timeout, [=]() {
		m_bDrawValue = false;
		update();
	});
	initValues();
	initValueDisplay();
}

void PKSlider::setScaleCalc(std::shared_ptr<ScaleCalc> scaleCalc) {
	this->m_pScaleCalc = scaleCalc;
	setMinimum(m_pScaleCalc->minAllowedValue());
	setMaximum(m_pScaleCalc->maxAllowedValue());
}

float PKSlider::minimum() const { return m_fMinimum; }

void PKSlider::setMinimum(float fMinimum) {
	m_fMinimum = fMinimum;
	initValues();
}

float PKSlider::maximum() const { return m_fMaximum; }

void PKSlider::setMaximum(float fMmaximum) {
	m_fMaximum = fMmaximum;
	initValues();
}

float PKSlider::value() const {
	if (m_pScaleCalc) {
		return m_pScaleCalc->decode(m_fValue);
	} else {
		return m_fValue;
	}
}

void PKSlider::setValue(float fValue) {
	if (m_pScaleCalc) {
		fValue = m_pScaleCalc->encode(fValue);
	}
	if (fValue > m_fMaximum)
		m_fValue = m_fMaximum;
	else if (fValue < m_fMinimum)
		m_fValue = m_fMinimum;
	else
		m_fValue = fValue;
	emit valueChanged(m_fValue);
	update();
}

QString PKSlider::unit() const { return m_sUnit; }

void PKSlider::setUnit(const QString &sUnit) {
	m_sUnit = sUnit;
	update();
}

int PKSlider::displayValueTimeout() const { return m_iDisplayValueTimeout; }

void PKSlider::setDisplayValueTimeout(int iValueDisplayTimeout) {
	m_iDisplayValueTimeout = iValueDisplayTimeout;
	initValueDisplay();
}

bool PKSlider::displayValue() const { return m_bDisplayValue; }

void PKSlider::setDisplayValue(bool bDisplayValue) {
	m_bDisplayValue = bDisplayValue;
	m_bDrawValue = (m_iDisplayValueTimeout == 0);
	initValueDisplay();
}

float PKSlider::pageStep() const { return m_fPageStep; }

void PKSlider::setPageStep(float fPageStep) { m_fPageStep = fPageStep; }

float PKSlider::singleStep() const { return m_fSingleStep; }

void PKSlider::setSingleStep(float fSingleStep) { m_fSingleStep = fSingleStep; }

int PKSlider::countMarkers() const { return m_iCountMarkers; }

void PKSlider::setCountMarkers(int iCountMarkers) {
	m_iCountMarkers = iCountMarkers;
	update();
}

float PKSlider::defaultValue() const { return m_fDefaultValue; }

void PKSlider::setDefaultValue(float fDefaultValue) {
	m_fDefaultValue = fDefaultValue;
}

bool PKSlider::drawMarker() const { return m_bDrawMarker; }

void PKSlider::setDrawMarker(bool bDrawStandardMarker) {
	m_bDrawMarker = bDrawStandardMarker;
	update();
}

QColor PKSlider::colorKnob() const { return m_ColorKnob; }

void PKSlider::setColorKnob(const QColor &ColorSurface) {
	m_ColorKnob = ColorSurface;
	m_ColorKnob.setAlpha(200);
	update();
}

void PKSlider::wheelEvent(QWheelEvent *event) {
	QPoint numDegrees = event->angleDelta() / 8;

	if (!m_RectBounds.contains(event->pos()))
		return;

	bool pageStep =
		(event->modifiers() & Qt::Modifier::CTRL) == Qt::Modifier::CTRL;

	if (!numDegrees.isNull()) {
		QPoint numSteps = numDegrees / 15;
		setValue(value() +
				 numSteps.y() * (pageStep ? m_fPageStep : m_fSingleStep));
	}

	event->accept();
}

void PKSlider::paintEvent(QPaintEvent *event) {
	QPainter painter(this);

	painter.fillPath(m_PathSlideSlot, QBrush(m_GradientSlot));

	if (m_bDrawMarker) {
		if (m_pScaleCalc && (m_pScaleCalc->vScaleValues().size() > 0))
			drawScalecalcScale(painter);
		else if (m_iCountMarkers > 0) {
			drawCustomScale(painter);
		}
	}

	drawCurrentValue(event, painter);
	if (m_bDrawValue)
		drawValueText(painter);
	painter.end();
}

void PKSlider::drawCurrentValue(__attribute__((unused)) QPaintEvent *event,
								QPainter &painter) {
	int pos = calculatePosition(m_fValue);
	int left = m_RectSlideSlot.x() + (m_RectSlideSlot.width() - m_iSliderWith);
	QRect sliderRect =
		QRect(left, pos - m_iSliderHeight / 2, m_iSliderWith, m_iSliderHeight);
	QPainterPath path(QPoint(left, pos));
	path.lineTo(m_RectSlideSlot.x(), sliderRect.y());
	path.lineTo(m_RectSlideSlot.x() + m_RectSlideSlot.width(), sliderRect.y());
	path.lineTo(m_RectSlideSlot.x() + m_RectSlideSlot.width(),
				sliderRect.y() + sliderRect.height());
	path.lineTo(m_RectSlideSlot.x() + m_RectSlideSlot.width(),
				sliderRect.y() + sliderRect.height());
	path.lineTo(m_RectSlideSlot.x(), sliderRect.y() + sliderRect.height());
	path.lineTo(QPoint(left, pos));
	painter.save();
	painter.setPen(m_ColorKnob);
	painter.setBrush(QBrush(m_ColorKnob));
	painter.drawPath(path);
	QPen pen = QPen(Qt::GlobalColor::white);
	pen.setWidth(2);
	painter.drawLine(left, pos, left + m_iSliderWith - 1, pos);
	painter.restore();
}

void PKSlider::drawCustomScale(QPainter &painter) {
	painter.save();
	QPen p = QPen(m_ColorMarker, m_iMarkerWidth);
	painter.setPen(p);
	int step = m_fValueRange / 10;
	float val = m_fMinimum;
	int left = m_RectSlideSlot.x() + (m_RectSlideSlot.width() - m_iSliderWith);
	QFont font = painter.font();
	font.setPointSize(m_iFontSizeValue);
	painter.setFont(font);
	QLine l(left, 0, m_RectSlideSlot.x() - 2, 0);
	for (int i = 0; i <= m_iCountMarkers; i++) {
		int pos = calculatePosition(val);
		painter.drawLine(l.translated(0, pos));
		if (m_bDrawMarkerValues)
			painter.drawText(m_RectSlideSlot.x() + m_RectSlideSlot.width() + 2,
							 pos + m_iFontSizeValue / 2, QString::number(val));
		val += step;
	}
	painter.restore();
}

void PKSlider::drawScalecalcScale(QPainter &painter) {
	painter.save();
	QPen p = QPen(m_ColorMarker, m_iMarkerWidth);
	painter.setPen(p);
	int left = m_RectSlideSlot.x() + (m_RectSlideSlot.width() - m_iSliderWith);
	QFont font = painter.font();
	font.setPointSize(m_iFontSizeValue);
	painter.setFont(font);
	QLine l(left, 0, m_RectSlideSlot.x() - 2, 0);
	for (float val : m_pScaleCalc->vScaleValues()) {
		int pos = calculatePosition(m_pScaleCalc->encodeScale(val));
		painter.drawLine(l.translated(0, pos));
		if (m_bDrawMarkerValues)
			painter.drawText(m_RectSlideSlot.x() + m_RectSlideSlot.width() + 2,
							 pos + m_iFontSizeValue / 2, QString::number(val));
	}
	painter.restore();
}

void PKSlider::drawValueText(QPainter &painter) {
	QString sVal;
	float fVal;
	if (m_pScaleCalc)
		fVal = m_pScaleCalc->decodeScale(m_fValue);
	else
		fVal = m_fValue;
	unsigned int pos = calculatePosition(m_fValue);
	sVal = QString::number(round(static_cast<double>(fVal)));
	if (m_sUnit != "")
		sVal += " " + m_sUnit;
	if (pos > m_iScaleRange / 2)
		pos -= m_iFontSizeValue * 3;
	else
		pos += m_iFontSizeValue;
	QRect fontRect(10, pos, m_RectSlideSlot.x() - 10, m_iFontSizeValue * 2);
	painter.save();
	QFont font = painter.font();
	font.setPointSize(m_iFontSizeValue);
	QColor c = m_ColorKnob.darker();
	c.setAlpha(220);
	painter.fillRect(fontRect, QBrush(c));
	painter.setFont(font);
	painter.drawText(fontRect, Qt::AlignVCenter | Qt::AlignHCenter, sVal,
					 &fontRect);
	painter.restore();
}

void PKSlider::mouseTimerElapsed() {
	setValue(m_fNextValue);
	emit sliderPressed();
}

void PKSlider::resizeEvent(__attribute__((unused)) QResizeEvent *event) {
	m_RectBounds = this->rect();
	int height = m_RectBounds.height() / 20;
	m_iSliderHeight = height < 6 ? 6 : height > 12 ? 12 : height;

	m_iFontSizeValue = m_iSliderHeight > 8 ? 8 : m_iSliderHeight;

	m_iBorderTop = m_iSliderHeight / 2;
	m_iBorderBottom = m_iBorderTop;

	m_RectSlideSlot = m_RectBounds.adjusted(
		(m_RectBounds.width() / 2) - m_iSlotWidth / 2, m_iBorderTop,
		((m_RectBounds.width() / 2) - m_iSlotWidth / 2) * -1, -m_iBorderBottom);

	m_iScaleRange = m_RectSlideSlot.height();

	m_GradientSlot =
		QLinearGradient(m_RectSlideSlot.x(), 0,
						m_RectSlideSlot.x() + m_RectSlideSlot.width(), 0);
	m_GradientSlot.setColorAt(0.4, m_color1Slot);
	m_GradientSlot.setColorAt(1, m_color2Slot);

	m_PathSlideSlot.addRoundedRect(
		m_RectSlideSlot.adjusted(0, -1 * (m_iSlotWidth / 2), 0,
								 m_iSlotWidth / 2),
		m_iSlotWidth / 2, m_iSlotWidth / 2);
	initValues();
}
void PKSlider::mousePressEvent(QMouseEvent *event) {
	if (m_RectBounds.contains(event->pos()) &&
		event->button() == Qt::MouseButton::LeftButton) {
		m_fNextValue = positionToValue(event->y());
		m_TimerMouse.start();
		event->accept();
	}
}

void PKSlider::mouseDoubleClickEvent(QMouseEvent *event) {
	if (m_RectBounds.contains(event->pos()) &&
		(event->button() == Qt::LeftButton)) {
		if (m_TimerMouse.isActive())
			m_TimerMouse.stop();
		setValue(m_fDefaultValue);
		event->accept();
	}
}

void PKSlider::mouseMoveEvent(QMouseEvent *event) {
	if (m_RectBounds.contains(event->pos())) {
		setValue(positionToValue(event->y()));
		event->accept();
	}
}

void PKSlider::initValues() {
	m_fValueRange = maximum() - minimum();
	m_fMapping = m_iScaleRange / m_fValueRange;
	update();
}

void PKSlider::initValueDisplay() {
	if ((m_iDisplayValueTimeout > 0) && m_bDisplayValue) {
		connect(this, &PKSlider::valueChanged, [=] {
			m_bDrawValue = true;
			update();
			m_TimerValue.start(m_iDisplayValueTimeout);
		});
		m_TimerValue.start(m_iDisplayValueTimeout);
	} else {
		m_bDrawValue = m_bDisplayValue;
	}
	update();
}

unsigned int PKSlider::calculatePosition(float value) {
	return m_iScaleRange - (value - m_fMinimum) * m_fMapping + m_iBorderTop;
}

float PKSlider::positionToValue(int pos) {
	if (pos > (m_RectSlideSlot.y() + m_RectSlideSlot.height()))
		pos = m_RectSlideSlot.y() + m_RectSlideSlot.height();
	float val =
		(m_iScaleRange - (pos - m_iBorderTop)) / m_fMapping + m_fMinimum;
	if (m_pScaleCalc) {
		val = m_pScaleCalc->decode(val);
	}
	return val;
}

void PKSlider::keyPressEvent(QKeyEvent *event) {
	float valDelta = 0;
	bool pageStep =
		(event->modifiers() & Qt::Modifier::CTRL) == Qt::Modifier::CTRL;
	switch (event->key()) {
	case Qt::Key_Plus:
		valDelta = pageStep ? m_fPageStep : m_fSingleStep;
		break;
	case Qt::Key_Minus:
		valDelta = (pageStep ? m_fPageStep : m_fSingleStep) * -1;
		break;
	case Qt::Key_PageUp:
		valDelta = m_fPageStep;
		break;
	case Qt::Key_PageDown:
		valDelta = m_fPageStep * -1;
	}
	setValue(value() + valDelta);
	event->accept();
}
