isEmpty( PK_CALC ) {
PK_CALC = 1

HEADERS +=  \
            $$PWD/calc/dbcalc.h \
			$$PWD/calc/scalecalc.h \
			$$PWD/calc/logcalc.h

SOURCES +=  \
            $$PWD/calc/dbcalc.cpp \
			$$PWD/calc/scalecalc.cpp \
			$$PWD/calc/logcalc.cpp

}
