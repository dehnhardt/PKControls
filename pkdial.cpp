#include "pkdial.h"

#include <cmath>
#include <iostream>

PKDial::PKDial(QWidget *parent) : QWidget(parent) {
	setFocusPolicy(Qt::StrongFocus);
	QPalette palette;
	m_ColorMarker = palette.color(QPalette::Active, QPalette::ButtonText);
	m_TimerMouse.setSingleShot(true);
	m_TimerMouse.setInterval(250);
	m_TimerValue.setSingleShot(true);
	connect(&m_TimerMouse, &QTimer::timeout, this, &PKDial::mouseTimerElapsed);
	connect(&m_TimerValue, &QTimer::timeout, [=]() {
		m_bDrawValue = false;
		update();
	});
	initValues();
	initValueDisplay();
}

void PKDial::setScaleCalc(std::shared_ptr<ScaleCalc> scaleCalc) {
	this->m_pScaleCalc = scaleCalc;
	setMinimum(m_pScaleCalc->minAllowedValue());
	setMaximum(m_pScaleCalc->maxAllowedValue());
}

void PKDial::paintEvent(QPaintEvent *event) {
	QPainter painter(this);
	QPalette palette;
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setPen(palette.color(QPalette::Active, QPalette::Shadow));
	QRadialGradient radialGrad(
		m_RectDial.center().x() - (m_RectDial.width() / 4),
		m_RectDial.center().y() - (m_RectDial.width() / 4),
		m_RectDial.width() / 1.5);
	radialGrad.setColorAt(0, m_ColorKnob);
	radialGrad.setColorAt(0.7, m_ColorKnob.lighter(180));
	radialGrad.setColorAt(1, m_ColorKnob.darker());
	painter.setBrush(radialGrad);
	painter.drawPath(m_DialPath);

	if (m_bDrawMarker) {
		if (m_pScaleCalc && (m_pScaleCalc->vScaleValues().size() > 0))
			drawScalecalcScale(painter);
		else if (m_iCountMarkers == 0) {
			drawDefaultScale(painter);
		} else {
			drawCustomScale(painter);
		}
	}
	painter.setBrush(m_ColorKnob);
	painter.drawPath(m_DialPath);
	if (m_bDrawNotch)
		drawCurrentValue(event, painter);
	if (m_bDrawValue)
		drawValueText(painter);
	painter.end();
}

void PKDial::drawCurrentValue(__attribute__((unused)) QPaintEvent *event,
							  QPainter &painter) {

	painter.save();
	painter.setRenderHint(QPainter::Antialiasing);
	painter.translate(m_RectDial.x() + m_RectDial.width() / 2,
					  m_RectDial.y() + m_RectDial.height() / 2);
	painter.rotate(calculateAngle(m_fValue) + m_iScaleAnchor);
	if (m_bDrawNotchLine) {
		QPen p = QPen(m_ColorNotch, m_iNotchLineWidth);
		painter.setPen(p);
		QLine l(0, static_cast<int>(m_RectDial.height() / 2.5), 0,
				m_RectDial.height() / 2 - m_iNotchLineWidth);
		painter.drawLine(l);
		p.setWidth(1);
		painter.setPen(p);
		painter.setBrush(m_ColorNotch);
		painter.drawEllipse(
			QPoint(0, m_RectDial.height() / 2 - m_iNotchLineWidth / 2),
			m_iNotchLineWidth / 2, m_iNotchLineWidth / 2);
	}
	painter.setPen(QColor(255, 0, 0));
	painter.setBrush(QColor(255, 0, 0));
	painter.drawEllipse(QPoint(0, m_RectDial.height() / 2 - m_iNotchLineWidth),
						m_iNotchMarkerWidth, m_iNotchMarkerWidth);
	painter.restore();
}

void PKDial::resizeEvent(__attribute__((unused)) QResizeEvent *event) {
	m_RectContent = this->rect();
	m_RectDial =
		m_RectContent.height() < m_RectContent.width()
			? QRect((m_RectContent.width() - m_RectContent.height()) / 2,
					m_RectContent.top(), m_RectContent.height(),
					m_RectContent.height())
			: QRect(m_RectContent.left(),
					(m_RectContent.height() - m_RectContent.width()) / 2,
					m_RectContent.width(), m_RectContent.width());
	m_PointCenter = m_RectContent.center();
	m_RectDial.adjust(2, 2, -2, -2);
	m_DialPath = QPainterPath();
	m_DialPath.addEllipse(m_RectDial);
	m_iNotchLineWidth =
		(m_RectDial.width() / 17) < 4 ? 4 : m_RectDial.width() / 17;

	m_bDrawNotchLine = (m_iNotchLineWidth > 4);
	if (m_bDrawNotchLine)
		m_iNotchMarkerWidth =
			(m_iNotchLineWidth / 3) < 2 ? 2 : m_iNotchLineWidth / 3;
	else
		m_iNotchMarkerWidth = m_RectDial.width() > 45 ? 3 : 2;
	m_iMarkerWidth =
		(m_RectDial.width() / 25) < 2 ? 2 : m_RectDial.width() / 25;
	m_iFontSizeValue = m_RectDial.width() < 40 ? 6 : this->font().pointSize();
}

void PKDial::initValues() {
	m_iScaleOffset = (360 - m_iScaleRange) / 2 + m_iScaleAnchor;
	m_fValueRange = maximum() - minimum();
	m_fMapping = m_iScaleRange / m_fValueRange;
	update();
}

void PKDial::initValueDisplay() {
	if ((m_iDisplayValueTimeout > 0) && m_bDisplayValue) {
		connect(this, &PKDial::valueChanged, [=] {
			m_bDrawValue = true;
			update();
			m_TimerValue.start(m_iDisplayValueTimeout);
		});
		m_TimerValue.start(m_iDisplayValueTimeout);
	} else {
		m_bDrawValue = m_bDisplayValue;
	}
	update();
}

double PKDial::calculateAngle(float val) {
	return static_cast<double>((val - m_fMinimum) * m_fMapping +
							   m_iScaleOffset);
}

float PKDial::angleToValue(float angle) {
	if (angle < m_iScaleOffset)
		angle = 0;
	else if (angle > m_iScaleOffset + m_iScaleRange)
		angle = m_iScaleRange;
	else
		angle -= m_iScaleOffset;

	float val = (angle / m_fMapping) + m_fMinimum;
	if (val > m_fMaximum)
		return m_fMaximum;
	else if (val < m_fMinimum)
		return m_fMinimum;
	if (m_pScaleCalc) {
		val = m_pScaleCalc->decode(val);
	}
	return val;
}

void PKDial::drawDefaultScale(QPainter &painter) {
	double angle = 0;
	painter.save();
	QPen p = QPen(m_ColorMarker, m_iMarkerWidth);
	painter.setPen(p);
	QLine l(0, static_cast<int>(m_RectDial.height() / 2.2), 0,
			m_RectDial.height() / 2);
	painter.translate(m_RectDial.x() + m_RectDial.width() / 2,
					  m_RectDial.y() + m_RectDial.height() / 2);
	angle = calculateAngle(m_fMinimum);
	painter.rotate(angle + m_iScaleAnchor);
	painter.drawLine(l);
	angle = calculateAngle(m_fValueRange / 2) - angle;
	painter.rotate(angle);
	painter.drawLine(l);
	angle = calculateAngle(m_fValueRange) - (angle + m_iScaleOffset);
	painter.rotate(angle);
	painter.drawLine(l);
	painter.restore();
}

void PKDial::drawCustomScale(QPainter &painter) {
	double angle = 0.;
	painter.save();
	QPen p = QPen(m_ColorMarker, m_iMarkerWidth);
	painter.setPen(p);
	QLine l(0, static_cast<int>(m_RectDial.height() / 2.2), 0,
			m_RectDial.height() / 2);
	painter.translate(m_RectDial.x() + m_RectDial.width() / 2,
					  m_RectDial.y() + m_RectDial.height() / 2);
	angle = calculateAngle(m_fMinimum);
	painter.rotate(angle + m_iScaleAnchor);
	painter.drawLine(l);
	angle = static_cast<double>(m_iScaleRange) / m_iCountMarkers;
	for (int i = 0; i < m_iCountMarkers; i++) {
		painter.rotate(angle);
		painter.drawLine(l);
	}
	painter.restore();
}

void PKDial::drawValueText(QPainter &painter) {
	QString sVal;
	float fVal;
	if (m_pScaleCalc)
		fVal = m_pScaleCalc->decodeScale(m_fValue);
	else
		fVal = m_fValue;
	sVal = QString::number(round(static_cast<double>(fVal)));
	if (m_sUnit != "")
		sVal += " " + m_sUnit;
	painter.save();
	QFont font = painter.font();
	font.setPointSize(m_iFontSizeValue);
	painter.setFont(font);
	painter.drawText(m_RectDial, Qt::AlignHCenter | Qt::AlignVCenter, sVal);
	painter.restore();
}

QString PKDial::unit() const { return m_sUnit; }

void PKDial::setUnit(const QString &sUnit) {
	m_sUnit = sUnit;
	update();
}

int PKDial::displayValueTimeout() const { return m_iDisplayValueTimeout; }

void PKDial::setDisplayValueTimeout(int iValueDisplayTimeout) {
	m_iDisplayValueTimeout = iValueDisplayTimeout;
	initValueDisplay();
}

bool PKDial::displayValue() const { return m_bDisplayValue; }

void PKDial::setDisplayValue(bool bDisplayValue) {
	m_bDisplayValue = bDisplayValue;
	m_bDrawValue = (m_iDisplayValueTimeout == 0);
	initValueDisplay();
}

void PKDial::drawScalecalcScale(QPainter &painter) {
	double angle = 0;
	painter.save();
	QPen p = QPen(m_ColorMarker, m_iMarkerWidth);
	painter.setPen(p);
	QLine l(0, static_cast<int>(m_RectDial.height() / 2.2), 0,
			m_RectDial.height() / 2);
	painter.translate(m_RectDial.x() + m_RectDial.width() / 2,
					  m_RectDial.y() + m_RectDial.height() / 2);
	int i = 0;
	for (float val : m_pScaleCalc->vScaleValues()) {
		i++;
		angle = calculateAngle(m_pScaleCalc->encodeScale(val));
		painter.rotate(angle + m_iScaleAnchor);
		painter.drawLine(l);
		painter.rotate((angle + m_iScaleAnchor) * -1);
	}
	painter.restore();
}

float PKDial::pageStep() const { return m_fPageStep; }

void PKDial::setPageStep(float fPageStep) { m_fPageStep = fPageStep; }

float PKDial::singleStep() const { return m_fSingleStep; }

void PKDial::setSingleStep(float fSingleStep) { m_fSingleStep = fSingleStep; }

int PKDial::countMarkers() const { return m_iCountMarkers; }

void PKDial::setCountMarkers(int iCountMarkers) {
	m_iCountMarkers = iCountMarkers;
	update();
}

bool PKDial::drawNotch() const { return m_bDrawNotch; }

void PKDial::setDrawNotch(bool bDrawNotch) {
	m_bDrawNotch = bDrawNotch;
	update();
}

void PKDial::mouseTimerElapsed() {
	setValue(m_fNextValue);
	emit dialPressed();
}

float PKDial::defaultValue() const { return m_fDefaultValue; }

void PKDial::setDefaultValue(float fDefaultValue) {
	m_fDefaultValue = fDefaultValue;
}

bool PKDial::drawMarker() const { return m_bDrawMarker; }

void PKDial::setDrawMarker(bool bDrawStandardMarker) {
	m_bDrawMarker = bDrawStandardMarker;
	update();
}

float PKDial::mousePositionToAngle(QMouseEvent *event) {
	QLineF l = QLineF(event->pos(), m_RectDial.center());
	double f2 = fmod((450 - m_iScaleAnchor) - l.angle(), 360.0);
	return static_cast<float>(f2);
}

QColor PKDial::colorKnob() const { return m_ColorKnob; }

void PKDial::setColorKnob(const QColor &ColorSurface) {
	m_ColorKnob = ColorSurface;
	m_ColorKnob.setAlpha(200);
	update();
}

float PKDial::value() const {
	if (m_pScaleCalc) {
		return m_pScaleCalc->decode(m_fValue);
	} else {
		return m_fValue;
	}
}

void PKDial::setValue(float fValue) {
	if (m_pScaleCalc) {
		fValue = m_pScaleCalc->encode(fValue);
	}
	if (fValue < m_fMinimum)
		m_fValue = m_fMinimum;
	else if (fValue > m_fMaximum)
		m_fValue = m_fMaximum;
	else
		m_fValue = fValue;
	emit valueChanged(m_fValue);
	update();
}

unsigned int PKDial::scaleOffset() const { return m_iScaleOffset; }

unsigned int PKDial::scaleRange() const { return m_iScaleRange; }

void PKDial::setScaleRange(unsigned int iScaleRange) {
	m_iScaleRange = iScaleRange;
	initValues();
	emit scaleRangeChanged(m_iScaleRange);
}

unsigned int PKDial::scaleAnchor() const { return m_iScaleAnchor; }

void PKDial::setScaleAnchor(unsigned int iScaleAnchor) {
	m_iScaleAnchor = iScaleAnchor;
	update();
	emit scaleAnchorChanged(m_iScaleAnchor);
}

float PKDial::maximum() const { return m_fMaximum; }

void PKDial::setMaximum(float fMaximum) {
	m_fMaximum = fMaximum;
	initValues();
	emit rangeChanged(m_fMinimum, m_fMaximum);
}

float PKDial::minimum() const { return m_fMinimum; }

void PKDial::setMinimum(float fMinimum) {
	m_fMinimum = fMinimum;
	initValues();
	emit rangeChanged(m_fMinimum, m_fMaximum);
}

void PKDial::wheelEvent(QWheelEvent *event) {
	QPoint numDegrees = event->angleDelta() / 8;

	if (!m_DialPath.contains(event->pos()))
		return;

	bool pageStep =
		(event->modifiers() & Qt::Modifier::CTRL) == Qt::Modifier::CTRL;

	if (!numDegrees.isNull()) {
		QPoint numSteps = numDegrees / 15;
		setValue(value() +
				 numSteps.y() * (pageStep ? m_fPageStep : m_fSingleStep));
	}

	event->accept();
}

void PKDial::mousePressEvent(QMouseEvent *event) {
	if (m_DialPath.contains(event->pos()) &&
		event->button() == Qt::MouseButton::LeftButton) {
		m_fNextValue = angleToValue(mousePositionToAngle(event));
		m_TimerMouse.start();
		event->accept();
	}
}

void PKDial::mouseDoubleClickEvent(QMouseEvent *event) {
	if (m_DialPath.contains(event->pos()) &&
		(event->button() == Qt::LeftButton)) {
		if (m_TimerMouse.isActive())
			m_TimerMouse.stop();
		setValue(defaultValue());
		event->accept();
	}
}

void PKDial::mouseMoveEvent(QMouseEvent *event) {
	if (m_DialPath.contains(event->pos())) {
		setValue(angleToValue(mousePositionToAngle(event)));
		event->accept();
	}
}

void PKDial::keyPressEvent(QKeyEvent *event) {
	float valDelta = 0;
	bool pageStep =
		(event->modifiers() & Qt::Modifier::CTRL) == Qt::Modifier::CTRL;
	switch (event->key()) {
	case Qt::Key_Plus:
		valDelta = pageStep ? m_fPageStep : m_fSingleStep;
		break;
	case Qt::Key_Minus:
		valDelta = (pageStep ? m_fPageStep : m_fSingleStep) * -1;
		break;
	case Qt::Key_PageUp:
		valDelta = m_fPageStep;
		break;
	case Qt::Key_PageDown:
		valDelta = m_fPageStep * -1;
	}
	setValue(value() + valDelta);
	event->accept();
}
